<?php

class WebScrapper 
{
    /**
     *
     *  @var string
     */
    private $_username;

    /**
     *
     *  @var string
     */
    private $_password;

    /**
     *
     *  @var string
     */
    private $_clientId;

    /**
     *
     *  @var string
     */
    private $_clientSecret;

    /**
     *
     *  @var string
     */
    private $_apiBaseURL;

    /**
     * Constructor method
     */
    public function __construct($username, $password, $clientId, $clientSecret)
    {
        $this->_username = $username;
        $this->_password = $password;
        $this->_clientId = $clientId;
        $this->_clientSecret = $clientSecret;
        $this->_apiBaseURL = 'https://api.k2s.cc/v1';
    }

    /**
     * To get account details
     * 
     * @return string
     */
    public function getAccountDetails()
    {
        // Login and get access tokens
        $accessTokens = $this->logInAndGetToken();
        if(!$accessTokens)
            return 'Unble to get access tokens. Please try again later.';
        
        $accountDetails = [];
        
        // Get profile details
        $profileDetails = $this->getProfileDetails($accessTokens);
        if(!$profileDetails || !$profileDetails->accountType)
            return 'Unable to get profile type. Please try again later.';
        $accountDetails['profileType'] = $profileDetails->accountType;

        // Get account quota
        $quotaDetails = $this->getQuotaDetails($accessTokens);
        if(!$quotaDetails || !$quotaDetails->dailyTraffic)
            return 'Unable to get daily quota statistics. Please try again later.';
        $accountDetails['trafficLeftToday'] = $quotaDetails->dailyTraffic->total - $quotaDetails->dailyTraffic->used;
        $accountDetails['trafficUsedToday'] = $quotaDetails->dailyTraffic->used;

        return json_encode($accountDetails);
    }

    /**
     * To log the user in and return access token
     * 
     * @return mixed
     */
    private function logInAndGetToken()
    {
        $requestBody = ['username' => $this->_username,
        "password"      =>  $this->_password,
        "grant_type"    =>  'password',
        "client_id"     =>  $this->_clientId,
        "client_secret" =>  $this->_clientSecret,
        "csrfToken"     =>  'SomeRandomString',

        ];

        return $this->sendAPIRequest($this->_apiBaseURL . '/auth/token/', 'POST', [], json_encode($requestBody));
    }

    /**
     * To get user profile details
     * 
     * @param $accessTokens
     * 
     * @return mixed
     */
    private function getProfileDetails($accessTokens)
    {
        $requestHeader = [
            'Cookie: __cfduid=SomeRandomText; accessToken=' . $accessTokens->access_token . '; refreshToken=' . $accessTokens->refresh_token
        ];

        return $this->sendAPIRequest($this->_apiBaseURL . '/users/me/', 'GET', $requestHeader);
    }

    /**
     * To get user daily quota
     * 
     * @param $accessTokens
     * 
     * @return mixed
     */
    private function getQuotaDetails($accessTokens)
    {
        $requestHeader = [
            'Cookie: __cfduid=SomeRandomText; accessToken=' . $accessTokens->access_token . '; refreshToken=' . $accessTokens->refresh_token
        ];

        return $this->sendAPIRequest($this->_apiBaseURL . '/users/me/statistic/', 'GET', $requestHeader);
    }

    /**
     * To send API request via CURL
     * 
     * @param $requestURL
     * @param $requestMethod
     * @param $requestHeader
     * @param $requestBody
     * 
     * @return mixed
     */
    private function sendAPIRequest($requestURL, $requestMethod, $requestHeader = [], $requestBody = null)
    {
        try{
            $curl = curl_init();

            $defaultRequestHeader = array(
                'Referer: https://k2s.cc/auth/login',
                'Origin: https://k2s.cc',
                'Content-Type: application/json;charset=UTF-8' 
            );
            $requestHeader = array_merge($requestHeader, $defaultRequestHeader);
            curl_setopt_array($curl, array(
                CURLOPT_URL => $requestURL,
                CURLOPT_CUSTOMREQUEST => $requestMethod,
                CURLOPT_POSTFIELDS => $requestBody,
                CURLOPT_HTTPHEADER => $requestHeader,
                CURLOPT_RETURNTRANSFER => true, 
            ));

            $apiResponse = curl_exec($curl);
            $response = false;
            if(curl_getinfo($curl, CURLINFO_HTTP_CODE) == 200){
                $response = json_decode($apiResponse);
            }
            curl_close($curl);

            return $response;
        } catch(Exception $e) {
            return false;
        }
    }
}